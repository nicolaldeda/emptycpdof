package com.qaagility.controller;

public class CntCounter {

    public int dCounter(int paramA, int paramB){
        if (paramB == 0){
            return Integer.MAX_VALUE;
        }else{
            return paramA / paramB;
        }
    }

}
